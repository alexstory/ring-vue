import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Inc from '@/components/Inc'
import Names from '@/components/Names'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/inc',
      component: Inc
    },
    {
      path: '/names',
      component: Names
    }
  ]
})
