import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    inc: {
      count: 0
    }
  },
  getters: {
    count: state => state.inc.count
  },
  mutations: {
    incCount (state) {
      state.inc.count ++
    },
    decCount (state) {
      state.inc.count --
    }
  }
})
