(ns ring-vue.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.util.response :refer [response]]
            [ring.middleware.json :refer [wrap-json-response]]))

(defroutes app-routes
  (GET "/" [] (slurp "resources/public/index.html"))
  (GET "/data" [] (response {:names ["autumn" "alex" "arabella"]}))
  (route/resources "/")
  (route/not-found (slurp "resources/public/index.html")))

(def app
  (wrap-json-response app-routes site-defaults))
